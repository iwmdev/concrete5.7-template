<?php  defined('C5_EXECUTE') or die("Access Denied."); ?>
<?php  Loader::library('authentication/open_id');?>
<?php  $form = Loader::helper('form'); ?>

<script type="text/javascript">
$(function() {
	$("input[name=uName]").focus();
});
</script>

<div class="row">
	<div class="col-xs-12">
		<span class="headerBorder clearfix"><h1><?php echo t('Sign in to %s', SITE)?></h1></span>
		<?php Loader::element('system_errors', array('error' => $error)); ?>
		<?php  if (isset($intro_msg)) { ?>
			<div class="alert alert-info alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<p><?php echo $intro_msg?></p>
			</div>
		<?php  } ?>
	</div>
</div>

<?php  if( $passwordChanged ){ ?>

	<div class="block-message info alert-message"><p><?php echo t('Password changed. Please login to continue. ') ?></p></div>

<?php  } ?> 

<?php  if($changePasswordForm){ ?>
<div class="row">
	<section class="change-password col-sm-6 col-sm-offset-3 well">

		<h4><?php echo t('Enter your new password below.') ?></h4>

		<form method="post" action="<?php echo $this->url( '/login', 'change_password', $uHash )?>" role="form"> 

			<div class="form-group">
				<label for="uPassword"><?php echo t('New Password')?></label>
				<input type="password" name="uPassword" id="uPassword" class="ccm-input-text form-control">
			</div>
			<div class="form-group">
				<label for="uPasswordConfirm"  class="control-label"><?php echo t('Confirm Password')?></label>
				<input type="password" name="uPasswordConfirm" id="uPasswordConfirm" class="ccm-input-text form-control">
			</div>

			<?php echo $form->submit('submit', t('Sign In') . ' &gt;','','blue-button'); ?>

		</form>

	</section>
</div>

<?php  } elseif($validated) { ?>
<div class="row">
	<section class="email-validated col-sm-6 col-sm-offset-3 well">
		<h4><?php echo t('Email Address Verified')?></h4>

		<p>
		<?php echo t('The email address <b>%s</b> has been verified and you are now a fully validated member of this website.', $uEmail)?>
		</p>
		
		<a class="btn gold-button" href="<?php echo $this->url('/')?>"><?php echo t('Continue to Site')?></a>

	</section>
</div>


<?php  } else if (isset($_SESSION['uOpenIDError']) && isset($_SESSION['uOpenIDRequested'])) { ?>

<div class="ccm-form">

<?php  switch($_SESSION['uOpenIDError']) {
	case OpenIDAuth::E_REGISTRATION_EMAIL_INCOMPLETE: ?>

		<form method="post" action="<?php echo $this->url('/login', 'complete_openid_email')?>">
			<p><?php echo t('To complete the signup process, you must provide a valid email address.')?></p>
			<label for="uEmail"><?php echo t('Email Address')?></label><br/>
			<?php echo $form->text('uEmail')?>
				
			<div class="ccm-button">
			<?php echo $form->submit('submit', t('Sign In') . ' &gt;')?>
			</div>
		</form>

	<?php  break;
	case OpenIDAuth::E_REGISTRATION_EMAIL_EXISTS:
	
	$ui = UserInfo::getByID($_SESSION['uOpenIDExistingUser']);
	
	?>

		<form method="post" action="<?php echo $this->url('/login', 'do_login')?>">
			<p><?php echo t('The OpenID account returned an email address already registered on this site. To join this OpenID to the existing user account, login below:')?></p>
			<label for="uEmail"><?php echo t('Email Address')?></label><br/>
			<div><strong><?php echo $ui->getUserEmail()?></strong></div>
			<br/>
			
			<div>
			<label for="uName"><?php  if (USER_REGISTRATION_WITH_EMAIL_ADDRESS == true) { ?>
				<?php echo t('Email Address')?>
			<?php  } else { ?>
				<?php echo t('Username')?>
			<?php  } ?></label><br/>
			<input type="text" name="uName" id="uName" <?php echo  (isset($uName)?'value="'.$uName.'"':'');?> class="ccm-input-text">
			</div>			<div>

			<label for="uPassword"><?php echo t('Password')?></label><br/>
			<input type="password" name="uPassword" id="uPassword" class="ccm-input-text">
			</div>

			<div class="ccm-button">
			<?php echo $form->submit('submit', t('Sign In') . ' &gt;')?>
			</div>
		</form>

	<?php  break;

	}
?>

</div>

<?php  } else if ($invalidRegistrationFields == true) { ?>

<div class="ccm-form">

	<p><?php echo t('You must provide the following information before you may login.')?></p>
	
<form method="post" action="<?php echo $this->url('/login', 'do_login')?>">
	<?php  
	$attribs = UserAttributeKey::getRegistrationList();
	$af = Loader::helper('form/attribute');
	
	$i = 0;
	foreach($unfilledAttributes as $ak) { 
		if ($i > 0) { 
			print '<br/><br/>';
		}
		print $af->display($ak, $ak->isAttributeKeyRequiredOnRegister());	
		$i++;
	}
	?>
	
	<?php echo $form->hidden('uName', Loader::helper('text')->entities($_POST['uName']))?>
	<?php echo $form->hidden('uPassword', Loader::helper('text')->entities($_POST['uPassword']))?>
	<?php echo $form->hidden('uOpenID', $uOpenID)?>
	<?php echo $form->hidden('completePartialProfile', true)?>

	<div class="ccm-button">
		<?php echo $form->submit('submit', t('Sign In'))?>
		<?php echo $form->hidden('rcID', $rcID); ?>
	</div>
	
</form>
</div>	

<?php  } else { ?>

<div class="row">
	<div class="signin-form col-sm-6">
		
		<form method="post" action="<?php echo $this->url('/login', 'do_login')?>" role="form">

			<fieldset>
				
				<legend><?php echo t('User Account')?></legend>
				<div class="form-group">
					<label for="uName">
						<?php
							if (USER_REGISTRATION_WITH_EMAIL_ADDRESS == true) {
								echo t('Email Address');
							} else {
								echo t('Username');
							}
						?>
					</label>
					<input type="text" name="uName" id="uName" <?php echo  (isset($uName)?'value="'.$uName.'"':'');?> class="ccm-input-text form-control">
				</div>
				<div class="form-group">
					<label for="uPassword"><?php echo t('Password')?></label>
					<input type="password" name="uPassword" id="uPassword" class="ccm-input-text form-control" />
				</div>

			</fieldset>

			<?php  if (OpenIDAuth::isEnabled()) { ?>
				<fieldset>

					<legend><?php echo t('OpenID')?></legend>

					<div class="form-group">
						<label for="uOpenID"><?php echo t('Login with OpenID')?>:</label>
						<input type="text" name="uOpenID" id="uOpenID" <?php echo  (isset($uOpenID)?'value="'.$uOpenID.'"':'');?> class="ccm-input-openid form-control">
					</div>
				</fieldset>
			<?php  } ?>


			<fieldset>

				<legend><?php echo t('Options')?></legend>

				<?php  if (isset($locales) && is_array($locales) && count($locales) > 0) { ?>
					<div>
						<label for="USER_LOCALE"><?php echo t('Language')?></label>
						<div class="controls"><?php echo $form->select('USER_LOCALE', $locales)?></div>
					</div>
				<?php  } ?>
				
				<div class="checkbox">
					<label><?php echo $form->checkbox('uMaintainLogin', 1)?> <span><?php echo t('Remain logged in to website.')?></span></label>
				</div>
				<?php  $rcID = isset($_REQUEST['rcID']) ? Loader::helper('text')->entities($_REQUEST['rcID']) : $rcID; ?>
				<input type="hidden" name="rcID" value="<?php echo $rcID?>" />
			
			</fieldset>

			<hr />

			<?php echo $form->submit('submit', t('Sign In') . ' &gt;', array('class' => 'blue-button'))?>

		</form>
	</div>

	<section class="col-sm-6">
		<div class="row">
			<div class="forgot-password col-sm-12">

				<form method="post" action="<?php echo $this->url('/login', 'forgot_password')?>" role="form">

					<legend><?php echo t('Forgot Your Password?')?></legend>

					<p><?php echo t("Enter your email address below. We will send you instructions to reset your password.")?></p>

					<input type="hidden" name="rcID" value="<?php echo $rcID?>" />
						
					<div>
						<label for="uEmail"><?php echo t('Email Address')?></label>
						<input type="text" name="uEmail" value="" class="ccm-input-text form-control" >
					</div>
					
					<hr />

					<?php echo $form->submit('submit', t('Reset and Email Password') . ' &gt;', array('class' => 'red-button')); ?>

				</form>
			</div>


			<?php  if (ENABLE_REGISTRATION == 1) { ?>
			<div class="register col-sm-12">
				<legend><?php echo t('Not a Member')?></legend>
				<p><?php echo t('Create a user account for use on this website.')?></p>
				<hr />
				<a class="btn gold-button" href="<?php echo $this->url('/register')?>"><?php echo t('Register here!')?></a>
			</div>
			<?php  } ?>
		</div>
	</section>
</div>
<?php  } ?>
