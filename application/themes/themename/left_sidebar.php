<?php
    defined('C5_EXECUTE') or die(_("Access Denied."));
    $view->inc('elements/header.php');
?>
            <section class="mainContent col-sm-8 col-sm-push-4">
                <?php
                $main = new Area('Main');
                $main->enableGridContainer();
                $main->display($c);
                ?>
            </section>
            <aside class="sidebar col-sm-4 col-sm-pull-8">
                <?php
                $this->inc('elements/sidebar.php');
                ?>
            </aside>
<?php
    $view->inc('elements/footer.php');
?>