<?php
    defined('C5_EXECUTE') or die(_("Access Denied."));
    $view->inc('elements/header.php');
?>
            <section class="mainContent col-sm-12">
                <?php
                $main = new Area('Main');
                $main->enableGridContainer();
                $main->display($c);
                ?>
            </section>
<?php
    $view->inc('elements/footer.php');
?>