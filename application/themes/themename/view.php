<?php
    defined('C5_EXECUTE') or die(_("Access Denied."));
    $view->inc('elements/header.php');
?>
            <section class="mainContent col-sm-12">
                <?php
                $a = new Area('Main');
                $a->enableGridContainer();
                $a->display($c);

                print $innerContent;

                $a = new Area('Main Extra');
                $a->enableGridContainer();
                $a->display($c);
                ?>
            </section>
<?php
    $view->inc('elements/footer.php');
?>