<?php
    defined('C5_EXECUTE') or die(_("Access Denied."));
    $this->inc('elements/header.php');
?>
            <section class="mainContent col-sm-8">
                <?php
                $main = new Area('Main');
                $main->enableGridContainer();
                $main->display($c);
                ?>
            </section>
            <aside class="sidebar col-sm-4">
                <?php
                    $this->inc('elements/sidebar.php');
                ?>
            </aside>
<?php
    $this->inc('elements/footer.php');
?>