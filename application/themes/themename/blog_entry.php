<?php
    defined('C5_EXECUTE') or die(_("Access Denied."));
    $view->inc('elements/header.php');
?>
            <section class="mainContent col-sm-8">
                <h1><?php echo $c->getCollectionName(); ?></h1> 
                <span class="metadata">
                    <?php 
                        $u = new User();
                        if ($u->isRegistered()) { ?>
                            <?php  
                            if (Config::get("ENABLE_USER_PROFILES")) {
                                $userName = '<a href="' . $this->url('/profile') . '">' . $u->getUserName() . '</a>';
                            } else {
                                $userName = $u->getUserName();
                            }
                        }
                        echo 'Posted by: <span class="post-author">' . $userName . ' on ' . $c->getCollectionDatePublic('F jS, Y') . '</a></span>';
                    ?>
                </span>
                <?php 
                $a = new Area('Main');
                $a->enableGridContainer();
                $a->display($c);
                ?>
            </section>
            <aside class="sidebar col-sm-4">
                <?php
                    $this->inc('elements/sidebar.php');
                ?>
            </aside>
<?php
    $view->inc('elements/footer.php');
?>