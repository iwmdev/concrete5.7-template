<?php
/**
 * Created by PhpStorm.
 * User: Kricir
 * Date: 5/22/15
 * Time: 11:13 AM
 */

namespace Application\Theme\Themename;
use Concrete\Core\Page\Theme\Theme

class PageTheme extends Theme {

    protected $pThemeGridFrameworkHandle = 'bootstrap3';

    /**
     * Register assets to be loaded in your theme
     */
    public function registerAssets() {

    }

    /**
     * Register block classes "Custom Templates"
     *
     * return Array
     */
    public function getThemeBlockClasses() {
//        return array(
//            'block_name' => array('block_class_name'),
//        );
    }

    /**
     * Register area classes
     *
     * return Array
     */
    public function getThemeAreaClasses() {
//        return array(
//            'Area Name' => array('area_class_name')
//        );
    }

    /**
     * Set block default templates
     *
     * return Array
     */
    public function getThemeBlockDefaultTemplates() {
//        return array(
//            'block_name' => 'block_template_name'
//        );
    }

    /**
     * Define additional image sizes
     *
     * return Array
     */
    public function getThemeResponsiveImageMap() {
//        return array(
//            'image_name' => 'screen_width_in_pixels'
//        );
    }

    /**
     * Register additional redactor classes "Styles"
     *
     * return Array
     */
    public function getThemeEditorClasses() {
//        return array(
//            array('title' => t('styles_title'), 'menuClass' => 'menu_title', 'spanClass' => 'span_class_name')
//        );
    }
}