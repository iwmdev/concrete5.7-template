<?php  defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
            </div>
        </div>
    </section>
    <footer>
    </footer>
    <script src="<?php echo $view->getThemePath(); ?>/javascripts/bootstrap.js"></script>

    <!-- or Load Individual Bootstrap Plugins

    <script src="<?php echo $view->getThemePath(); ?>/javascripts/bootstrap-affix.js"></script>
    <script src="<?php echo $view->getThemePath(); ?>/javascripts/bootstrap-alert.js"></script>
    <script src="<?php echo $view->getThemePath(); ?>/javascripts/bootstrap-button.js"></script>
    <script src="<?php echo $view->getThemePath(); ?>/javascripts/bootstrap-carousel.js"></script>
    <script src="<?php echo $view->getThemePath(); ?>/javascripts/bootstrap-collapse.js"></script>
    <script src="<?php echo $view->getThemePath(); ?>/javascripts/bootstrap-dropdown.js"></script>
    <script src="<?php echo $view->getThemePath(); ?>/javascripts/bootstrap-modal.js"></script>
    <script src="<?php echo $view->getThemePath(); ?>/javascripts/bootstrap-popover.js"></script>
    <script src="<?php echo $view->getThemePath(); ?>/javascripts/bootstrap-scrollspy.js"></script>
    <script src="<?php echo $view->getThemePath(); ?>/javascripts/bootstrap-tab.js"></script>
    <script src="<?php echo $view->getThemePath(); ?>/javascripts/bootstrap-tooltip.js"></script>
    <script src="<?php echo $view->getThemePath(); ?>/javascripts/bootstrap-transition.js"></script>
    <script src="<?php echo $view->getThemePath(); ?>/javascripts/bootstrap-typeahead.js"></script>
    -->

    <!-- Additional Scripts -->
    </div>
<?php Loader::element('footer_required'); ?>
</body>
</html>