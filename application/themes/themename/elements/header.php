<?php  defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<!DOCTYPE html>
<html lang="<?php echo Localization::activeLanguage()?>">
<head>
	<?php Loader::element('header_required'); ?>
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
	<meta name="apple-mobile-web-app-capable" content="yes" />

	<!-- Styles -->
    <link href="<?php echo $html->css($view->getStylesheet('bootstrap/bootstrap.less')); ?>" rel="stylesheet" media="screen" />
    <link href="<?php echo $html->css($view->getStylesheet('styles.less')); ?>" rel="stylesheet" media="screen" />

	<!-- Scripts -->

</head>
<body>
    <div class="<?php echo $c->getPageWrapperClass(); ?>"></div>
	<header>
	</header>
	<section class="trunk">
		<div class="container">
			<div class="row">