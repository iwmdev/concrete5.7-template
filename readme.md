#Project Details

Edit this paragraph to add any notes needed for the project. To edit this area edit the readme.md file in the root level of the document.

##New Project Instructions

For detailed information please refer to the [C5 Template Wiki](https://bitbucket.org/iwmdev/c5-template/wiki/Home).

###Create Server Account

+ Sign into webmin
+ Click "Create Virtual Server"
+ New Virtual Server Details:
    + Domain Name: {_client user name_}_.web_{##}_.iwmstage.com_
    + Description: _Not Needed_
    + Administration Password: _Random 8 characters_
    + Click "Create Server"
    
###Create Sub Server Account

+ Select "{_client user name_}_.web_{##}_.iwmstage.com_" from dropdown menu
+ Click "Create Virtual Server"
+ Click "Sub-server"
+ New Virtual Server Details:
    + Domain Name: staging.{_client user name_}_.web_{##}_.iwmstage.com_
    + Description: _Not Needed_
    + Click "Create Server"

###Fork C5 Template Repository

+ Log into Bitbucket.org
+ Select "C5 Template" Repository
+ Under "Options" select "Fork"
+ Fork Settings:
    + Owner: _Set to "iwmdev"_ 
    + Name: _Set to "XXXXX Client Name"_
    + Description: _Not Needed_
    + Access Level: "Check" for Private
    + Forking: _Allow only private forks_
    + Project Management:
        + Issue Tracking: _Leave Checked_
        + Wiki: _Leave Checked_
+ Click "Fork repository"

###Create "Development" Branch

+ Log into Bitbucket.org
+ Select "C5 Template" Repository
+ Under "Options" select "Create Branch"
+ Branch Settings:
    + Branch From: _Master_
    + Branch Name: _Development_
    
###Clone and Checkout Repository

####Find Client Folder

+ Browse "Jobs" to find the client folder
+ In the client folder browse to the "XXXXX Client Name" folder

####Open Terminal

+ Open Terminal _(Mac)_ / Command _(Windows)_ and browse to the "XXXXX Client Name" Directory
   _You can also drag the folder from the finder window into the terminal window to have it load the directory for you_  
   __cd this/working/directory/to/SITE__

####Clone Vagrant Files

+ Clone Repo "_XXXXX - Client Name_"  
  In Terminal type:
    
        git clone git@bitbucket.org:imwdev/"XXXXX-Client-Name.git" SITE
        
####Checkout Development

+ Checkout Development Branch  
  In Terminal type:
    
        git checkout development

####Start Vagrant

+ Open Terminal _(Mac)_ / Command _(Windows)_ and browse to the "SITE" Directory you just created  
   _You can also drag the folder from the finder window into the terminal window to have it load the directory for you_  
   __cd this/working/directory/to/SITE__
+ In Terminal type:
   
        vagrant up
        
+ For additional Vagrant Information please view the [Wiki/Vagrant](https://bitbucket.org/iwmdev/c5-template/wiki/Vagrant)
+ You should now be able to visit [localhost:8080](http://localhost:8080) to view your new vagrant box  
  _You may need to edit the port number which will be in the terminal_

####Setup Deployment

Please refer to the documentation in the [Wiki/Deployment](https://bitbucket.org/iwmdev/c5-template/wiki/Deployment#markdown-header-how-to-use-dploy) to correctly set up the Deployment Process.

###Commit Initial Push

####Add Files

To add all the files created by the `vagrant up` command:

+ Open Terminal _(Mac)_ / Command _(Windows)_ and browse to the "SITE" Directory you just created  
   _You can also drag the folder from the finder window into the terminal window to have it load the directory for you_  
   __cd this/working/directory/to/SITE__
+ In Terminal type:
   
        git add .

####Commit Files

+ Open Terminal _(Mac)_ / Command _(Windows)_ and browse to the "SITE" Directory you just created  
   _You can also drag the folder from the finder window into the terminal window to have it load the directory for you_  
   __cd this/working/directory/to/SITE__
+ In Terminal type:
   
        git commit -m "Initial Commit"
        
####Push All Files

+ Open Terminal _(Mac)_ / Command _(Windows)_ and browse to the "SITE" Directory you just created  
   _You can also drag the folder from the finder window into the terminal window to have it load the directory for you_  
   __cd this/working/directory/to/SITE__
+ In Terminal type:
   
        git push

